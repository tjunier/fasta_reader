require "./spec_helper"
include FastaReader

describe FastaReader do
  records = [] of FastaRecord

  describe "#each_fasta" do
    FastaReader.each_fasta("spec/test.fasta") do |rec|
      records << rec
    end
    it "has the right number of records" do
      records.size.should eq 3
    end
    it "has the right header for record 1" do
      records[0][:hdr].should eq "ID_01"
    end
    it "has the right sequence for record 1" do
      records[0][:seq].should eq "atgatgatgcttcttctt"
    end
    it "has the right header for record 2" do
      records[1][:hdr].should eq "ID 02"
    end
    it "has the right sequence for record 2" do
      records[1][:seq].should eq "cgatcgacgatcgatgcacgatgaacctacgagcartcgagcatcagagcagcatcgagcagctacgagatt"
    end
    it "has the right header for record 3" do
      records[2][:hdr].should eq "XPQ_LATCH|Latimeria chalumnae unknown protein"
    end
    it "has the right sequence for record 3" do
      records[2][:seq].should eq "MERTHSNSGDKTSTVSLDVLSRHSHSSVNSTSDHTSASKRSVHDRFHSTCSCM"
    end
    uc_records = [] of FastaRecord
    FastaReader.each_fasta("spec/test.fasta", uppercase: true) do |rec|
      uc_records << rec
    end
    it "has the right number of records" do
      uc_records.size.should eq 3
    end
    it "has the right header for record 1" do
      uc_records[0][:hdr].should eq "ID_01"
    end
    it "has the right sequence for record 1" do
      uc_records[0][:seq].should eq "ATGATGATGCTTCTTCTT"
    end
    it "has the right header for record 2" do
      uc_records[1][:hdr].should eq "ID 02"
    end
    it "has the right sequence for record 2" do
      uc_records[1][:seq].should eq "CGATCGACGATCGATGCACGATGAACCTACGAGCARTCGAGCATCAGAGCAGCATCGAGCAGCTACGAGATT"
    end
    it "has the right header for record 3" do
      uc_records[2][:hdr].should eq "XPQ_LATCH|Latimeria chalumnae unknown protein"
    end
    it "has the right sequence for record 3" do
      uc_records[2][:seq].should eq "MERTHSNSGDKTSTVSLDVLSRHSHSSVNSTSDHTSASKRSVHDRFHSTCSCM"
    end
  end

  describe "#each_fasta_with_index" do

  indices = [] of Int32
  records.clear
  FastaReader.each_fasta_with_index("spec/test.fasta") do |rec, i|
    records << rec
    indices << i
  end
    it "has the right number of records" do
      records.size.should eq 3
    end
    it "has the right header for record 1" do
      records[0][:hdr].should eq "ID_01"
    end
    it "has the right sequence for record 1" do
      records[0][:seq].should eq "atgatgatgcttcttctt"
    end
    it "has the correct index for record 1" do
      indices[0].should eq 0
    end
    it "has the right header for record 2" do
      records[1][:hdr].should eq "ID 02"
    end
    it "has the right sequence for record 2" do
      records[1][:seq].should eq "cgatcgacgatcgatgcacgatgaacctacgagcartcgagcatcagagcagcatcgagcagctacgagatt"
    end
    it "has the correct index for record 2" do
      indices[1].should eq 1
    end
    it "has the right header for record 3" do
      records[2][:hdr].should eq "XPQ_LATCH|Latimeria chalumnae unknown protein"
    end
    it "has the right sequence for record 3" do
      records[2][:seq].should eq "MERTHSNSGDKTSTVSLDVLSRHSHSSVNSTSDHTSASKRSVHDRFHSTCSCM"
    end
    it "has the correct index for record 3" do
      indices[2].should eq 2
    end
  end


  describe "#all_fasta" do
  records = FastaReader.all_fasta("spec/test.fasta")
    it "has the right number of records" do
      records.size.should eq 3
    end
    it "has the right header for record 1" do
      records[0][:hdr].should eq "ID_01"
    end
    it "has the right sequence for record 1" do
      records[0][:seq].should eq "atgatgatgcttcttctt"
    end
    it "has the right header for record 2" do
      records[1][:hdr].should eq "ID 02"
    end
    it "has the right sequence for record 2" do
      records[1][:seq].should eq "cgatcgacgatcgatgcacgatgaacctacgagcartcgagcatcagagcagcatcgagcagctacgagatt"
    end
    it "has the right header for record 3" do
      records[2][:hdr].should eq "XPQ_LATCH|Latimeria chalumnae unknown protein"
    end
    it "has the right sequence for record 3" do
      records[2][:seq].should eq "MERTHSNSGDKTSTVSLDVLSRHSHSSVNSTSDHTSASKRSVHDRFHSTCSCM"
    end
  end

  describe "error handling" do
    it "raises a UnexpectedResidueException" do
      expect_raises(UnexpectedResidueException, "Unexpected residue character \":\" at line 11") do
        FastaReader.all_fasta("spec/wrong.fasta")
      end
    end
  end

end
