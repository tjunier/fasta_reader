# A module for reading Fasta files.
#
module FastaReader
  VERSION = "0.1.0"

  extend self

  class UnexpectedResidueException < Exception
  end

  # A simple structure consisting of the Fasta header (without the leading '>')
  # and the sequence, both as `String`s
  #
  alias FastaRecord = NamedTuple(hdr: String, seq: String)

  # Yields a FastaRecord for each record in the Fasta file whose name is passed
  # as argument. The file is closed automatically after being read.
  #
  # If 'uppercase' is set to true, the residue symbols are folded to uppercase.
  #
  # Further versions should take a File or even an IO object, allowing users to
  # read Fasta from strings or input streams.
  #
  def each_fasta(fasta_fname : String, uppercase : Bool = false, &block : FastaRecord -> Nil)
    File.open(fasta_fname) do |fasta|
      header = nil
      sequence = ""
      lineno = 1
      fasta.each_line do |line|
        first_char = line[0, 1]
        case first_char
        when ">"
          if header
            sequence = sequence.upcase if uppercase
            rec = {hdr: header[1..], seq: sequence}
            yield rec
          end
          header = line
          sequence = ""
        when /[a-zA-Z-]/
          sequence = sequence.not_nil! + line
        else
          raise UnexpectedResidueException.new "Unexpected residue character \"#{first_char}\" at line #{lineno}"
        end
        lineno += 1
      end
      # yield the last entry
      if header
        sequence = sequence.upcase if uppercase
        rec = {hdr: header[1..], seq: sequence}
        yield rec
      end
    end
  end

  def each_fasta_with_index(fasta_fname : String)
    i = 0
    each_fasta(fasta_fname) do |rec|
      yield rec, i
      i += 1
    end
  end

  def all_fasta(fasta_fname : String) : Array(FastaRecord)
    all_records = [] of FastaRecord
    each_fasta(fasta_fname) do |rec|
      all_records << rec
    end
    all_records
  end
end
